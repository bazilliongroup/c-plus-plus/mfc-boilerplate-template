//01/16/2010 C. Germany Visual Studio 2008 MFC Template
/*
     Notes: This is a MFC template to create graphical C++ apps without
	        using the Visual Studio 2008 wizards. It is stripped down to
			a bare minimum of code so apps are as simplified as possible.
			You must disable some automated features and manually set a
			few properties for projects using this template.

			1. Create an EMPTY Win32 project. Click -> "File" -> -> "New"
			   -> "Project" -> "Win32" -> "Win32 Project".
		    2. Name is and select a directory.
			3. Select "Application Settings" -> "Empty Project" then click "Finish".
			4. Rt-click project, select "Properties" -> "Configuration Properties"
			   -> "General" -> "Use of MFC" and change it to 
			   "Use MFC in a Static Library". This will give you MFC components.
		    5. Disable Incremental Linking. Rt-click project, select "Properties" 
			   -> "Configuration Properties" -> "Linker" -> "General" ->
			   "Enable Incremental Linking" and set it to "NO".
                    6. Add a resources file. Rt-click resources and add a DIALOG object.
                    7. Change the enumerated constant to match the Dialog name.
		    8. Note that unlike 2003, when calling SetWindowText() in 2008 the
			   string passed in must be cast/converted using "L".	  

https://www.youtube.com/watch?v=Su1x9EA5Njk&index=1&list=PLinNvyf9ULFW-POkn2kU-MAQIVln0Zp8s
https://www.youtube.com/watch?v=P7d6ZrktCEo&list=PLinNvyf9ULFW-POkn2kU-MAQIVln0Zp8s&index=2

*/
//-----------------------------------------------------------------------------------------
#include <afxwin.h>      //MFC core and standard components
#include "resource.h"    //main symbols
//-----------------------------------------------------------------------------------------

//Globals
CEdit* pINPUT;
CEdit* pOUTPUT;
CButton* pSTART; 
CButton* pCOMMIT;

class GAME_FORM : public CDialog
{

public:

    GAME_FORM(CWnd* pParent = NULL): CDialog(GAME_FORM::IDD, pParent)
    {    }

    // Dialog Data, name of dialog form
    enum{IDD = INTERFACE1};

protected:
    virtual void DoDataExchange(CDataExchange* pDX) { CDialog::DoDataExchange(pDX); }

    //Called right after constructor. Initialize things here.
    virtual BOOL OnInitDialog() 
    { 
            CDialog::OnInitDialog();

			//Initilaize all Resource items in INITIALIZE.RC
			//Everything works via pointers.
			pINPUT = (CEdit *)GetDlgItem( CE_INPUT );
			pOUTPUT = (CEdit *)GetDlgItem( CE_OUTPUT );
			pSTART = (CButton *)GetDlgItem( CB_START );
			pCOMMIT = (CButton *)GetDlgItem( CB_COMMIT );

			pINPUT->SetWindowTextW(L"Type HERE!");
			pOUTPUT->SetWindowTextW(L"Click \"START\" to begin !");
			pCOMMIT->EnableWindow( false );

            return true; 
    }

public:

//-----------------------------------------------------------------
	afx_msg void start() { STARTBUTTON(); }
	afx_msg void commit() { COMMITBUTTON(); }
//-----------------------------------------------------------------

	void STARTBUTTON()
	{
		MessageBox(L"Input text and hit [Commit]");
		pSTART->EnableWindow( false );		//Disables the Start Button
		pOUTPUT->Clear();					//Clear the output
		pCOMMIT->EnableWindow( true );		//Enable the Button

	}

	void COMMITBUTTON()
	{
		CString str = _T("");
		pINPUT->GetWindowText( str );
		pOUTPUT->SetWindowTextW( str );
	}


DECLARE_MESSAGE_MAP()
};

//-----------------------------------------------------------------------------------------

class TheGame : public CWinApp
{
public:
	TheGame() {  }

public:

	virtual BOOL InitInstance()
	{
		CWinApp::InitInstance();				// Call the Parent class instance
		GAME_FORM dlg;							// Building an instance on the stack
		m_pMainWnd = &dlg;						// m_pMainWnd is Microsoft's Wizard code
		INT_PTR nResponse = dlg.DoModal();		// DoModal pops up your resource.
		return FALSE;

	} //close function

};

//-----------------------------------------------------------------------------------------
//Need a Message Map Macro for both CDialog and CWinApp

BEGIN_MESSAGE_MAP(GAME_FORM, CDialog)
	ON_COMMAND( CB_START, start )
	ON_COMMAND( CB_COMMIT, commit )
END_MESSAGE_MAP()

//-----------------------------------------------------------------------------------------

TheGame theApp;  //Starts the Application